FROM ujwal19/alpine-py3-java8

# File Author / Maintainer
MAINTAINER Prajwal Shreyas

# install sdk for alpine linux
RUN apk add --update --no-cache \
    python3-dev \
    build-base

#Install jupyter kernel gateway
#RUN pip install jupyter_kernel_gateway

# Copy the application folder inside the container
ADD /app-ftse-stream /app-ftse-stream

# Get pip to download and install requirements:
RUN pip install -r /app-ftse-stream/requirements.txt

# Expose ports
EXPOSE 8005

# Set the default directory where CMD will execute
WORKDIR /app-ftse-stream
CMD python3 /app-ftse-stream/FTSE_prices.py