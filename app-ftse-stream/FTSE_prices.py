# -*- coding: utf-8 -*-
"""
Created on Mon Oct 10 06:03:52 2016

@author: prajwalshreyas
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Oct  7 20:37:48 2016

@author: prajwalshreyas
"""

from googlefinance import getQuotes
import json
import pandas as pd
import boto3
from apscheduler.schedulers.blocking import BlockingScheduler
import pymysql
import logging
import config
from config import *

logging.basicConfig()

log = logging.getLogger('apscheduler.executors.default')
log.setLevel(logging.INFO)  # DEBUG
fmt = logging.Formatter('%(levelname)s:%(name)s:%(message)s')

#s3 = boto3.client('s3')
#s3.download_file('singularity-resource-1', 'rds_config.csv', 'streaming-prices-ftse100/rds_config.csv')
rds_config = pd.DataFrame()
rds_config = pd.read_csv('streaming-prices-ftse100/rds_config.csv')

rds_host  = rds_config['db_path'][0]
name = rds_config['db_username'][0]
password = rds_config['db_password'][0]
db_name = rds_config['db_name'][0]
port = 3306


    
def get_prices():
    
    try:
        conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, connect_timeout=5)
        print(conn)
        cursor = conn.cursor()
    except:
        print('Error to Connect to SQL DB')
       
    
    # Get SP prices CSV file from S3
    #s3 = boto3.client('s3')
    #s3.download_file('singularity-resource-1', 'SP500.csv', '/home/ec2-user/prices/SP500.csv') 

    #Covert the csv to dataframe
    stockList_DF = pd.DataFrame()
    stockList_DF = pd.read_csv('streaming-prices-ftse100/ftse100.csv')
    
    ticker_name = stockList_DF['ticker']

    
    ticker_name_1 = ticker_name[:99]


    prices_SP = pd.read_json(json.dumps(getQuotes(ticker_name_1), indent=2))
    prices_SP_filtered = prices_SP[['LastTradeDateTime','LastTradePrice','StockSymbol']]
    
    SP_list = prices_SP_filtered.values.tolist()
    
    #Add the data to the mySQL database
    #prices_SP_filtered.to_sql('prices',conn,flavor='mysql', if_exists='append', index= False)
    for row in SP_list:
        
        date = row[0]
        stock = row[2]
        s_price = row[1]
        
        try:
           

             cursor.execute('''INSERT INTO prices (LastTradeDateTime, StockSymbol, Price) VALUES (%s,%s,%s);''',([date], [stock], [s_price]))
         
        except Exception as e:
           print (e.message, e.args) 
           continue
    conn.commit()
    conn.close()          
            
    return 0
 
#get_prices()        
scheduler = BlockingScheduler()
scheduler.add_job(get_prices,'cron', day_of_week='mon-fri',hour='9-16', minute='*/5')
#scheduler.add_job(get_prices,'interval',seconds=10)
scheduler.start()
# Logging
h = logging.StreamHandler()
h.setFormatter(fmt)
log.addHandler(h)

